export RAP_ID="def-stbil30"
module load mugqic/genpipes/3.1.4

mkdir -p $SCRATCH/sb_cofactor/A549/output/chip-pipeline-POL2RA_Myers_GRCh38

$MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.py -j slurm -s '1-15' \
    -l debug \
    -r $SCRATCH/sb_cofactor/A549/raw/ENCODE-chip/readset_POLR2A_20191021.txt \
    -d $SCRATCH/sb_cofactor/A549/raw/ENCODE-chip/design_POLR2A_20191021.txt \
    -o $SCRATCH/sb_cofactor/A549/output/chip-pipeline-POL2RA_Myers_GRCh38 \
    --config $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/chipseq/chipseq.cedar.ini \
        $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini \
        input/chipseq.numpy.bug.ini \
        input/cedar_M35.txt
