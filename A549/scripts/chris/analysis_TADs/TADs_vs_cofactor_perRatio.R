# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ComplexHeatmap)
library(circlize)
source("scripts/chris/load_chrStructure.R")
source("scripts/ckn_utils.R")

#####
countCBDSInTad <- function(tads, diffbind) {
  tad_id <- paste(seqnames(tads), start(tads), end(tads), sep = "_")
  df <- data.frame("tad_id" = tad_id)
  
  for (set in names(diffbind)) {
    regions <- diffbind[[set]]
    countInTad <- countOverlaps(tads, regions)
    df <- cbind(df, countInTad)
  }
  colnames(df) <- c("tad_id", names(diffbind))
  return(df)
}

######
computeRatio <- function(vec_up, vec_down) {
  res <- rep("TMP", length(vec_up))
  for (i in 1:length(vec_up)) {
    val_up <- vec_up[i]
    val_down <- vec_down[i]
    if (val_up == 0 & val_down == 0) {
      res[i] = -1
    } else {
      res[i] = val_up/(val_up+val_down)
    }
  }
  return(as.numeric(res))
}

######
saveHeatmap <- function(heatmap, output_dir, output_file, width_val = 25, height_val = 22, format = "pdf") {
  output_filepath <- file.path(output_dir, paste0(output_file, ".", format))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(heatmap)
  dev.off()
  message(" > Heatmap saved in ", output_filepath)
}

# today
today <- get_today()

# Output dir
outputdir <- "output/analyses/ecosystem/heatmap_cofactor_perTad"

# Load diffbind
diffbind <- load_diffbind_cofactors_peaks()

##### Parameters
# res <- "5000"
# res <- "10000"
# res <- "25000"
 res <- "50000"
with_clustering <- TRUE
# COF <- "MBCNS"
# COF <- "MBC"
COF <- "MB"

# TADs
TADs <- load_TADs(timepoint = "1h", resolution = res, method_norm = "VC_SQRT")
n_tad <- length(TADs)
countTad <- countCBDSInTad(TADs, diffbind)

countTad_ratio <- countTad %>% mutate(MED1_R = computeRatio(MED1_UP, MED1_DOWN),
                                      BRD4_R = computeRatio(BRD4_UP, BRD4_DOWN),
                                      CDK9_R = computeRatio(CDK9_UP, CDK9_DOWN),
                                      NIPBL_R = computeRatio(NIPBL_UP, NIPBL_DOWN),
                                      SMC1A_R = computeRatio(SMC1A_UP, SMC1A_DOWN))

if (COF == "MBCNS") {
  countTad_ratio <- countTad_ratio %>% dplyr::select(tad_id, MED1_R, BRD4_R, CDK9_R, NIPBL_R, SMC1A_R) %>% 
    arrange(desc(MED1_R), desc(BRD4_R), desc(CDK9_R), desc(NIPBL_R), desc(SMC1A_R))
  n_no_binding <- sum(rowSums(countTad_ratio[, -1]) == -5)
} else if (COF == "MBC") {
  countTad_ratio <- countTad_ratio %>% dplyr::select(tad_id, MED1_R, BRD4_R, CDK9_R) %>% 
    arrange(desc(MED1_R), desc(BRD4_R), desc(CDK9_R))
  n_no_binding <- sum(rowSums(countTad_ratio[, -1]) == -3)
} else if (COF == "MB") {
  countTad_ratio <- countTad_ratio %>% dplyr::select(tad_id, MED1_R, BRD4_R) %>% 
    arrange(desc(MED1_R), desc(BRD4_R))
  n_no_binding <- sum(rowSums(countTad_ratio[, -1]) == -2)
}

countTad_ratio_mat <- countTad_ratio %>% dplyr::select(-tad_id) %>% as.matrix

customColors = colorRamp2(c(-1, -0.1, 0, 0.5, 1), c("black", "black", "#1E8449", "white", "#800020"))

hm <- Heatmap(countTad_ratio_mat, name = "ratio up | (up+down)",
              cluster_rows = with_clustering,
              show_row_names = FALSE,
              cluster_columns = FALSE,
              column_dend_side = "bottom",
              col = customColors,
              column_names_side = "top", column_names_rot = 45,
              na_col = "black",
              row_title = paste0(n_tad-n_no_binding, " / ", n_tad, "\nwith\ncofactor\nbinding"),
              row_title_rot = 0,
              column_title = paste("TADs", "(Resolution", "=", res, "bp)"),
              column_title_gp = gpar(fontsize = 20, fontface = "bold"))
hm

if (with_clustering == TRUE) {
  outputfile <- paste(today, "heatmap", "CBDS", "perTAD", "ratio", paste0("res", res), COF, "wClustering", sep = "_")
} else {
  outputfile <- paste(today, "heatmap", "CBDS", "perTAD", "ratio", paste0("res", res), COF, paste0("rankedBy", COF), sep = "_")
}

# saveHeatmap(hm, output_dir = outputdir, output_file = outputfile, width_val = 7, height_val = 9)

