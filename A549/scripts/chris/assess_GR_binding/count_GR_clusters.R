# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ggplot2)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

# Load GR binding
all_gr_regions <- load_reddy_gr_binding_consensus()
sapply(all_gr_regions[1:5], length)
sapply(all_gr_regions[6:16], length)

# Aggregate GR binding per clusters
clustered_gr_regions <- lapply(all_gr_regions, GenomicRanges::reduce, min.gapwidth=25000)
all_gr_regions <- load_reddy_gr_binding_consensus()
sapply(clustered_gr_regions[1:5], length)
sapply(clustered_gr_regions[6:16], length)

# Between 0 and 25 minutes
tp_0_25m <- c(0, 5, 10, 15, 20, 25)
nb_0_25m <- c(0, sapply(all_gr_regions[1:5], length))
nb_0_25mbis <- c(0, sapply(clustered_gr_regions[1:5], length))
df_0_25m <- data.frame(timepoint = rep(tp_0_25m, 2), nb = c(nb_0_25m, nb_0_25mbis),
                       GR = rep(c("raw", "clustered"), each = length(tp_0_25m)))

ggplot(df_0_25m, aes(x = timepoint, y = nb, color = GR)) +
  geom_line(size=1.5, alpha=0.9) +
  geom_point(shape=23, color="#C0C0C0", fill="#FFD700", size=6) +
  ylab("Number of peaks") +
  xlab("Time (minute)") +
  theme_minimal()
  
# Between 0 and 12h
tp_0_12h <- c(0, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12)
nb_0_12h <- c(0, sapply(all_gr_regions[6:16], length))
nb_0_12hbis <- c(0, sapply(clustered_gr_regions[6:16], length))
df_0_12h <- data.frame(timepoint = tp_0_12h, nb = c(nb_0_12h, nb_0_12hbis),
                       GR = rep(c("raw", "clustered"), each = length(tp_0_12h)))
ggplot(df_0_12h, aes(x = timepoint, y = nb, color = GR)) +
  geom_line(size=1.5, alpha=0.9) +
  geom_point(shape=23, color="#C0C0C0", fill="#FFD700", size=6) +
  ylab("Number of peaks") +
  xlab("Time (hour)") +
  scale_x_continuous(breaks = tp_0_12h, labels = tp_0_12h) +
  scale_y_continuous(breaks = c(2000, 4000, 6000, 8000, 10000, 12000), limit = c(0, 12000)) +
  theme_minimal()
˜