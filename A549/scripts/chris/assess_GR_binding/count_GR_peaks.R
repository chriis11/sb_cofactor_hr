# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ggplot2)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

# Load GR binding
all_gr_regions <- load_reddy_gr_binding_consensus()
sapply(all_gr_regions[1:5], length)
sapply(all_gr_regions[6:16], length)

# Between 0 and 25 minutes
tp_0_25m <- c(0, 5, 10, 15, 20, 25)
nb_0_25m <- c(0, sapply(all_gr_regions[1:5], length))
df_0_25m <- data.frame(timepoint = tp_0_25m, nb = nb_0_25m)

ggplot(df_0_25m, aes(x = timepoint, y = nb)) +
  geom_line(color="#22427C", size=1.5, alpha=0.9) +
  geom_point(shape=23, color="#C0C0C0", fill="#FFD700", size=6) +
  ylab("Number of peaks") +
  xlab("Time (minute)") +
  theme_minimal()
  
# Between 0 and 12h
tp_0_12h <- c(0, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12)
nb_0_12h <- c(0, sapply(all_gr_regions[6:16], length))
df_0_12h <- data.frame(timepoint = tp_0_12h, nb = nb_0_12h)

ggplot(df_0_12h, aes(x = timepoint, y = nb)) +
  geom_line(color="#22427C", size=1.5, alpha=0.9) +
  geom_point(shape=23, color="#C0C0C0", fill="#FFD700", size=6) +
  ylab("Number of peaks") +
  xlab("Time (hour)") +
  scale_x_continuous(breaks = tp_0_12h, labels = tp_0_12h) +
  scale_y_continuous(breaks = c(2000, 4000, 6000, 8000, 10000, 12000), limit = c(0, 12000)) +
  theme_minimal()

# Integrate both dataset
tp_bis <- c(0, 5, 10, 15, 20, 25, 30, 60, 120, 180, 240, 300, 360, 420, 480, 600, 720)
nb_bis <- c(0, sapply(all_gr_regions, length))
df_bis <- data.frame(timepoint = tp_bis, nb = nb_bis)

ggplot(df_bis, aes(x = timepoint, y = nb)) +
  geom_line(color="#22427C", size=1.5, alpha=0.9) +
  geom_point(shape=23, color="#C0C0C0", fill="#FFD700", size=2) +
  ylab("Number of peaks") +
  xlab("Time (hour)") +
  scale_x_continuous(breaks = tp_bis, labels = c(0, "", "", "", "", "", 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12)) +
  theme_minimal()



