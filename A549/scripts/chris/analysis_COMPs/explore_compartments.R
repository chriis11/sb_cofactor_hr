# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(plyr)
source("scripts/chris/load_chrStructure.R")

COMP_500kb <- load_compartmentsXY(timepoint = "1h", resolution = "500000", method_norm = "VC_SQRT")
length(COMP_500kb)
summary(width(COMP_500kb))
perchr_500kb <- as.data.frame(COMP_500kb) %>% group_by(seqnames, compartment) %>% tally
kable(perchr_500kb)

COMP_1000kb <- load_compartmentsXY(timepoint = "1h", resolution = "1000000", method_norm = "VC_SQRT")
length(COMP_1000kb)
summary(width(COMP_1000kb))
perchr_1000kb <- as.data.frame(COMP_1000kb) %>% group_by(seqnames, compartment) %>% tally
kable(perchr_1000kb)

COMP_2500kb <- load_compartmentsXY(timepoint = "1h", resolution = "2500000", method_norm = "VC_SQRT")
length(COMP_2500kb)
summary(width(COMP_2500kb))
perchr_2500kb <- as.data.frame(COMP_2500kb) %>% group_by(seqnames, compartment) %>% tally
kable(perchr_2500kb)

# Better to determine which one ar A and B