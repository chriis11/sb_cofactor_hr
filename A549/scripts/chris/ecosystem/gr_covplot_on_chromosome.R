# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(ChIPseeker)
library(grid)
library(gridExtra)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

##### What's the day today?
today <- get_today()

###
saveCovPlot <- function(covplot, output_dir, output_file, width_val = 25, height_val = 22) {
  output_filepath <- file.path(output_dir, paste0(output_file, ".pdf"))
  pdf(file = output_filepath, width = width_val, height = height_val)
  print(covplot)
  dev.off()
  message(" > Covplot saved in ", output_filepath)
}

# Get chromosomes sizes
chrom_sizes_raw <- read_tsv("input/hg38.chrom.sizes", col_names = FALSE)
colnames(chrom_sizes_raw) <- c("chrom", "size")
std_chr <- paste0("chr", c(1:22, "X", "Y"))
chrom_sizes <- chrom_sizes_raw %>% dplyr::filter(chrom %in% std_chr)

#
output_covplot = "output/analyses/ecosystem/GR_covplot"

#
all_gr_regions <- load_reddy_gr_binding_consensus()
names(all_gr_regions)
sapply(all_gr_regions, length)

for (chr in std_chr) {
  covplot_chr_list <- list()
  for (time in names(all_gr_regions)) {
    peaks <- all_gr_regions[[time]]
    
    title_covplot_chr <- paste("At", time, "|", length(peaks[seqnames(peaks) == chr]), "regions",
                               "\nDistribution of GR binding sites on", chr, sep = " ")
    
    chromSize <- chrom_sizes %>% filter(chrom == chr) %>% pull(size)

    plotcov_chr <- covplot(peaks, chrs = chr, title = title_covplot_chr,
                           xlab = "", lower = 0.1, xlim = c(0, chromSize))
    
    covplot_chr_list[[time]] <- plotcov_chr
  }
  saveCovPlot(grid.arrange(covplot_chr_list[["5 minutes"]], covplot_chr_list[["10 minutes"]],
                           covplot_chr_list[["15 minutes"]], covplot_chr_list[["20 minutes"]],
                           covplot_chr_list[["25 minutes"]], covplot_chr_list[["30 minutes"]],
                           covplot_chr_list[["1 hour"]], covplot_chr_list[["2 hours"]],
                           covplot_chr_list[["3 hours"]], covplot_chr_list[["4 hours"]],
                           covplot_chr_list[["5 hours"]], covplot_chr_list[["6 hours"]],
                           covplot_chr_list[["7 hours"]], covplot_chr_list[["8 hours"]],
                           covplot_chr_list[["10 hours"]], covplot_chr_list[["12 hours"]],
                           nrow = 16),
              output_dir = output_covplot, output_file = paste(today, "GR", "covplot_diffbind", chr, sep = "_"),
              width_val = 100, height_val = 25)
}
