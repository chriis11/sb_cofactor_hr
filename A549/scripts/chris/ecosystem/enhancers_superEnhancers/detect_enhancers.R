# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

###########
# From Lyu et al., Molecular Cell, 2018
# doi: 10.1016/j.molcel.2018.07.012
# Enhancer definition : 
# Enhancers were defined by using H3K4me1 peaks without H3K4me3 but overlapping ATAC-seq THSSs peaks,
# TSS ± 1 kb was excluded. Among these enhancers, those overlapping H3K27ac   peaks were taken
# as active enhancers, those overlapping with H3K27me3 peaks were taken as poised enhancers, 
# those overlapping with neither H3K27ac nor H3K27me3 peaks were considered primed enhancers.
# But Reddy didn't generate H3K27me3 ChIP-seq
###########

##### Export to GFF file for super_enhancers identification with ROSE (Rank-Ordering of Super-Enhancers)
export_for_ROSE <- function(enhancers, output_dir, output_filename) {
  enhancers_raw <- as.data.frame(enhancers) %>% dplyr::select(seqnames, start, end, strand)
  enhancers_raw$uniqueID <- paste(enhancers_raw$seqnames, enhancers_raw$start, enhancers_raw$end, sep = "_")
  enhancers_raw$strand <- "."
  enhancers_raw$col3 <- "."
  enhancers_raw$col6 <- "."
  enhancers_raw$col8 <- "."
  enhancers <- enhancers_raw %>% dplyr::select(seqnames, uniqueID, col3, start, end, col6, strand, col8, uniqueID)
  output_path <- file.path(output_dir, output_filename)
  write_tsv(enhancers, path = output_path, col_names = FALSE)
  message("Enhancer coordinates saved in ", output_path)
}

##### Get TSS
tss_raw <- promoters(most_expressed_TxDb, upstream = 1000, downstream = 1000)
tss <- keepStdChr(tss_raw)

##### Get chromatin marks and THSSs peaks
h3k4me1_raw <- load_reddy_binding_consensus("H3K4me1")
h3k4me3_raw <- load_reddy_binding_consensus("H3K4me3")
h3k27ac_raw <- load_reddy_binding_consensus("H3K27ac")
thss_raw <- load_reddy_thss_consensus()

h3k4me1_stdchr <- lapply(h3k4me1_raw, keepStdChr)
h3k4me3_stdchr <- lapply(h3k4me3_raw, keepStdChr)
h3k27ac_stdchr <- lapply(h3k27ac_raw, keepStdChr)
thss_stdchr <- lapply(thss_raw, keepStdChr)

sapply(h3k4me1_stdchr, length)
sapply(h3k4me3_stdchr, length)
sapply(h3k27ac_stdchr, length)
sapply(thss_stdchr, length)

##### Enhancers at 0 hour
h3k4me1_0h <- h3k4me1_stdchr[["0 hour"]] # 54985
h3k4me3_0h <- h3k4me3_stdchr[["0 hour"]] # 20106
thss_0h <- thss_stdchr[["0 hour"]] # 64851
h3k27ac_0h <- h3k27ac_stdchr[["0 hour"]] # 43776

enh_0h_a <- subsetByOverlaps(h3k4me1_0h, h3k4me3_0h, invert = TRUE) # 46100
enh_0h_b <- subsetByOverlaps(enh_0h_a, thss_0h) # 23063

enh_0h <- subsetByOverlaps(enh_0h_b, tss, invert = TRUE) # 22620
active_enh_0h <- subsetByOverlaps(enh_0h, h3k27ac_0h) # 16660
p_enh_0h <- subsetByOverlaps(enh_0h, h3k27ac_0h, invert = TRUE) # 5960

# 54985 > 46100 > 23063 > 22610
# enh: 22620 = 16660 + 5960

##### Enhancers at 1 hour
h3k4me1_1h <- h3k4me1_stdchr[["1 hour"]] # 39590
h3k4me3_1h <- h3k4me3_stdchr[["1 hour"]] # 20395
thss_1h <- thss_stdchr[["1 hour"]] # 66209
h3k27ac_1h <- h3k27ac_stdchr[["1 hour"]] # 45143

enh_1h_a <- subsetByOverlaps(h3k4me1_1h, h3k4me3_1h, invert = TRUE) # 36640
enh_1h_b <- subsetByOverlaps(enh_1h_a, thss_1h) # 19866

enh_1h <- subsetByOverlaps(enh_1h_b, tss, invert = TRUE) # 19565  
active_enh_1h <- subsetByOverlaps(enh_1h, h3k27ac_1h) # 14980
p_enh_1h <- subsetByOverlaps(enh_1h, h3k27ac_1h, invert = TRUE) # 4585

# 39590 > 36640 > 19866 > 19565 
# 19565: 14980 + 4585

##### Gathering enhancers at 0 and 1 hour
enh <- c(enh_0h, enh_1h)
active_enh <- c(active_enh_0h, active_enh_1h)
p_enh <- c(p_enh_0h, p_enh_1h)

##### Saving in an object
enhancers <- list("enh_0h" = enh_0h, "active_enh_0h" = active_enh_0h, "p_enh_0h" = p_enh_0h,
                  "enh_1h" = enh_1h, "active_enh_1h" = active_enh_1h, "p_enh_1h" = p_enh_1h,
                  "enh" = enh, "active_enh" = active_enh, "p_enh" = p_enh)
saveRDS(enhancers, file = "output/analyses/ecosystem/enhancers/enhancers.rds")

##### Export to a gff file
output_dir_ecosystem <- "output/analyses/ecosystem/enhancers"
export_for_ROSE(enh_0h, output_dir = output_dir_ecosystem, output_filename = "enhancers_0h.gff")
export_for_ROSE(enh_1h, output_dir = output_dir_ecosystem, output_filename = "enhancers_1h.gff")

##### Convert MED1_CTRL in gff format
artefact <- GRanges(seqnames = "chr3", ranges = IRanges(start = 93470340, end = 93470809))
med1_ctrl_raw <- load_cofactor_stdchr_peaks("MED1")[["MED1_CTRL"]]
med1_ctrl <- subsetByOverlaps(med1_ctrl_raw, artefact, invert = TRUE)
export_for_ROSE(med1_ctrl, output_dir = output_dir_ecosystem, output_filename = "med1_0h.gff")

##### Overlaps with cofactor differential binding sites
# cofactors <- c("MED1", "BRD4", "CDK9", "NIPBL", "SMC1A")
# cofactors <- c("BRD4")
# for (cofactor in cofactors) {
#   message("############")
#   message("#    ", cofactor)
#   message("############")
#   
#   regionSets <- diffbind[grep(cofactor, names(diffbind))]
#   print(sapply(regionSets, length))
#   
#   linear_annot <- lapply(regionSets, annotatePeaks, tss = 1000, TxDb = most_expressed_TxDb)
#   
#   for (sets in names(regionSets)) {
#     regions <- regionSets[[sets]]
#     # regions <- as.data.frame(regions) %>% dplyr::filter(abs(score) >= 1) %>% GRanges
#     
#     nb_peaks <- length(regions)
#     message("##### ", sets, " | ", nb_peaks, " peaks")
#     
#     # sets_annot <- annotatePeaks(regions, tss = 1000, TxDb = most_expressed_TxDb)
#     sets_annot <- linear_annot[[sets]]
#     distal_intergenic <- sets_annot # %>% dplyr::filter(Annot == "Distal Intergenic")
#     message("    # Annotated as distal intergenic : ", nrow(distal_intergenic))
#     
#     not_tss <- subsetByOverlaps(GRanges(distal_intergenic), tss)
#     message("    # At the TSS : ", length(not_tss))
#     message("")
#     
#     ov_enh0 <- subsetByOverlaps(GRanges(distal_intergenic), enh_0h) %>% length
#     message("    # Overlaps with enhancers_0h : ", ov_enh0 , " / ", nrow(distal_intergenic), " | ", round(ov_enh0/nrow(distal_intergenic)*100, 2), " %")
#     ov_active_enh0 <- subsetByOverlaps(GRanges(distal_intergenic), active_enh_0h) %>% length
#     message("    # Overlaps with active_enhancers_0h : ", ov_active_enh0 , " / ", nrow(distal_intergenic), " | ", round(ov_active_enh0/nrow(distal_intergenic)*100, 2), " %")
#     ov_p_enh0 <- subsetByOverlaps(GRanges(distal_intergenic), p_enh_0h) %>% length
#     message("    # Overlaps with p_enhancers_0h : ", ov_p_enh0 , " / ", nrow(distal_intergenic), " | ", round(ov_p_enh0/nrow(distal_intergenic)*100, 2), " %")
#     message("")
#     
#     ov_enh1 <- subsetByOverlaps(GRanges(distal_intergenic), enh_1h) %>% length
#     message("    # Overlaps with enhancers_1h : ", ov_enh1 , " / ", nrow(distal_intergenic), " | ", round(ov_enh1/nrow(distal_intergenic)*100, 2), " %")
#     ov_active_enh1 <- subsetByOverlaps(GRanges(distal_intergenic), active_enh_1h) %>% length
#     message("    # Overlaps with active_enhancers_1h : ", ov_active_enh1 , " / ", nrow(distal_intergenic), " | ", round(ov_active_enh1/nrow(distal_intergenic)*100, 2), " %")
#     ov_p_enh1 <- subsetByOverlaps(GRanges(distal_intergenic), p_enh_1h) %>% length
#     message("    # Overlaps with p_enhancers_1h : ", ov_p_enh1 , " / ", nrow(distal_intergenic), " | ", round(ov_p_enh1/nrow(distal_intergenic)*100, 2), " %")
#     message("")
#     
#     ov_enh <- subsetByOverlaps(GRanges(distal_intergenic), enh) %>% length
#     message("    # Overlaps with enhancers : ", ov_enh , " / ", nrow(distal_intergenic), " | ", round(ov_enh/nrow(distal_intergenic)*100, 2), " %")
#     ov_active_enh <- subsetByOverlaps(GRanges(distal_intergenic), active_enh) %>% length
#     message("    # Overlaps with active_enhancers : ", ov_active_enh , " / ", nrow(distal_intergenic), " | ", round(ov_active_enh/nrow(distal_intergenic)*100, 2), " %")
#     ov_p_enh <- subsetByOverlaps(GRanges(distal_intergenic), p_enh) %>% length
#     message("    # Overlaps with p_enhancers : ", ov_p_enh , " / ", nrow(distal_intergenic), " | ", round(ov_p_enh/nrow(distal_intergenic)*100, 2), " %")
#     message("")
#   }
# }
