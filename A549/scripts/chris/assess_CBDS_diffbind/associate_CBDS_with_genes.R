# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(tidyverse)
library(GenomicInteractions)
library(ChIPseeker)
source("scripts/ckn_utils.R")
source("scripts/load_reddy.R")

#####
annotatePeaks3D_1h <- function(promoter_regions, regulatory_regions) {
  annotation.features <- list(promoter = promoter_regions, enhancer = regulatory_regions)
  hic_1h <- readRDS("output/analyses/annotate_peaks_with_hic/hic_1h_GIObject.rds")
  annotateInteractions(hic_1h, annotation.features)
  return(hic_1h)
}

##### Load CBDS
diffbind <- load_diffbind_cofactors_peaks()

##### Load annotation database
# gff_file <- "/Users/chris/Downloads/gencode.v31.basic.annotation.gff3"
# # gff_file <- "/home/chris/Téléchargements/gencode.v31.basic.annotation.gff3"
# gencode31_basic_chr_TxDb <- makeTxDbFromGFF(file = gff_file, format = c("gff3"),
#                                             dataSource = "Gencode_v31",
#                                             organism = "Homo sapiens",
#                                             taxonomyId = 9606)
raw_promoters_A549 <- promoters(gencode31_basic_chr_TxDb, upstream = 3000, downstream = 3000, columns = "gene_id")
stdchr_promoters_A549 <- keepStdChr(raw_promoters_A549)
ensg_without_version <- str_replace(unlist(stdchr_promoters_A549$gene_id), "\\.[0-9]+$", "")
stdchr_promoters_A549$gene_id <- ensg_without_version
names(stdchr_promoters_A549) <- stdchr_promoters_A549$gene_id

##### Load categories
deg <- readRDS(file = "output/analyses/deg0_6_fdr1.rds")
upreg <- deg$gene_list$FC0p5$upreg
downreg <- deg$gene_list$FC0p5$downreg
length(upreg)
unique(length(upreg))
length(downreg)
unique(length(downreg))

##### For plot
tp <- c(0, 5, 10, 15, 20, 25, 30, 60, 120, 180, 240, 300, 360, 420, 480, 600, 720)
nb_a <- c(0)
pc_a <- c(0)
nb_r <- c(0)
pc_r <- c(0)

cbds_sets <- list()
for (set in names(diffbind)) {
  regions <- diffbind[[set]]
  names(regions) <- paste0(set, "_", 1:length(regions))
  message("#####################################")
  message("### ", set, " ( ", length(regions), " regions )")
  message("#####################################")
  
  #####################
  # Linear annotation
  #####################
  
  annodf <- annotatePeaks(regions, tss = 3000, TxDb_db = gencode31_basic_chr_TxDb)
  at_promoters <- annodf %>% dplyr::filter(Annot %in% c("Promoter"))
  # message("     Number of regions (at promoters) : ", nrow(at_promoters))
  genes_sets_ID <- at_promoters %>% dplyr::select(ENSG, SYMBOL) %>% unique
  colnames(genes_sets_ID) <- c("gene", "symbol")
  # message("     Number of unique genes : ", nrow(genes_sets_ID))
  annotated_cbds <- c(names(regions)[annodf$Annot == "Promoter"])
  
  # activated/repressed over 6h
  res <- cbind(genes_sets_ID, "upreg" = genes_sets_ID$gene %in% upreg, "downreg" = genes_sets_ID$gene %in% downreg)
  nb_upreg <- sum(res$upreg)
  nb_downreg <- sum(res$downreg)
  # message("     Associated with ",  nb_upreg, " induced genes > ", round(nb_upreg/nrow(res)*100, 2), " %")
  # message("     Associated with ",  nb_downreg, " repressed genes > ", round(nb_downreg/nrow(res)*100, 2), " %")
  # message(" ")
  
  #####################
  # 3D annotation
  #####################
  hic_3d <- annotatePeaks3D_1h(promoter_regions = stdchr_promoters_A549, regulatory_regions = regions)
  
  # Focus on enhancer-promoter
  ep <- hic_3d[isInteractionType(hic_3d, "enhancer", "promoter")]
  # message("     Number of GR sites linked to a promoter : ", length(ep))
  
  # Retrieve gene name from promoters
  prom1 <- anchorOne(ep)$promoter.id %>% unlist %>% na.omit %>% unique
  prom2 <- anchorTwo(ep)$promoter.id %>% unlist %>% na.omit %>% unique
  prom12 <- c(prom1, prom2) %>% unique %>% as.data.frame
  colnames(prom12) <- c("gene")
  # message("     Number of unique genes : ", nrow(prom12))
  annotated_cbds <- c(annotated_cbds,
                      anchorOne(ep)$enhancer.id %>% unlist %>% na.omit %>% unique,
                      anchorTwo(ep)$enhancer.id %>% unlist %>% na.omit %>% unique)
  
  
  # activated/repressed over 12h
  res_3D <- cbind(prom12, "upreg" = prom12$gene %in% upreg, "downreg" = prom12$gene %in% downreg)
  nb_upreg_3D <- sum(res_3D$upreg)
  nb_downreg_3D <- sum(res_3D$downreg)
  # message("     Associated with ",  nb_upreg_3D, " induced genes > ", round(nb_upreg_3D/nrow(res_3D)*100, 2), " %")
  # message("     Associated with ",  nb_downreg_3D, " repressed genes > ", round(nb_downreg_3D/nrow(res_3D)*100, 2), " %")
  # message(" ")
  
  ########################
  # INTEGRATING LINEAR AND 3D ANNOTATION
  ########################
  linear <- genes_sets_ID$gene
  message("\tLinear : ", length(linear))
  a3D <- as.character(prom12$gene)
  message("\t3D : ", length(a3D))
  linear_3D <- unique(c(linear, a3D))
  message("\tLinear + 3D : ", length(linear_3D))
  res_linear_3D <- data.frame(linear_3D, "upreg" = linear_3D %in% upreg, "downreg" = linear_3D %in% downreg)
  message("     Associated with ",  sum(res_linear_3D$upreg), " induced genes > ", round(sum(res_linear_3D$upreg)/length(upreg)*100, 2), " %")
  message("     Associated with ",  sum(res_linear_3D$downreg), " repressed genes > ", round(sum(res_linear_3D$downreg)/length(downreg)*100, 2), " %")
  message("")
  
  ##########
  # Gather numbers for plots
  ##########
  nb_a <- c(nb_a, sum(res_linear_3D$upreg))
  pc_a <- c(pc_a, round(sum(res_linear_3D$upreg)/length(upreg)*100, 2))
  nb_r <- c(nb_r, sum(res_linear_3D$downreg))
  pc_r <- c(pc_r, round(sum(res_linear_3D$downreg)/length(downreg)*100, 2))
  
  # ##########
  # # Gather all genes bounded by Gr over the time course
  # ##########
  # boundByGR <- c(boundByGR, linear_3D)
  
  # ##########
  # Count number of annotated CBDS regions
  # ##########
  unique_annotated_cbds <- unique(annotated_cbds)
  message("Number of annotated CBDS : ", length(unique(annotated_cbds)),
          " / ", length(regions), " | ",
          round(length(unique(annotated_cbds))/length(regions)*100, 2))
  message("")
  
  unique_unannotated_cbds <- names(regions)[!(names(regions) %in% unique_annotated_cbds)]
  
  cbds_sets[[set]]$anno <- unique_annotated_cbds
  cbds_sets[[set]]$not_anno <- unique_unannotated_cbds
}

# What are the unannotated CBDS?
peakAnnoList_anno <- list()
peakAnnoList_notAnno <- list()
for (set in names(cbds_sets)) {
  regions <- diffbind[[set]]
  names(regions) <- paste0(set, "_", 1:length(regions))
  
  cbds_anno <- regions[cbds_sets[[set]]$anno]
  cbds_notAnno <- regions[cbds_sets[[set]]$not_anno]
  
  csAnno_cbds_anno <- annotatePeaks(cbds_anno, tss = 3000, output = "anno", TxDb_db = gencode31_basic_chr_TxDb)
  peakAnnoList_anno[[set]] <- csAnno_cbds_anno

  csAnno_cbds_notAnno <- annotatePeaks(cbds_notAnno, tss = 3000, output = "anno", TxDb_db = gencode31_basic_chr_TxDb)
  peakAnnoList_notAnno[[set]] <- csAnno_cbds_notAnno
}

plotAnnoBar(peakAnnoList_anno, title = "Feature distribution of annotated CBDS")
plotAnnoBar(peakAnnoList_notAnno, title = "Feature distribution of unannotated CBDS")

plotDistToTSS(peakAnnoList_anno, title = "Distribution of annotated CBDS relative to TSS")
plotDistToTSS(peakAnnoList_notAnno, title = "Distribution of unannotated CBDS relative to TSS")

###### Stats
# boundByGR_unique <- unique(boundByGR)
# resTotal <- data.frame(boundByGR_unique, "upreg" = boundByGR_unique %in% upreg, "downreg" = boundByGR_unique %in% downreg)
# message("     Number of genes bound by GR : ", nrow(resTotal))
# message("     Associated with ",  sum(resTotal$upreg), " induced genes > ", round(sum(resTotal$upreg)/length(upreg)*100, 2), " %")
# message("     Associated with ",  sum(resTotal$downreg), " repressed genes > ", round(sum(resTotal$downreg)/length(downreg)*100, 2), " %")
# message("     Number of activated genes : ", length(upreg))
# message("     Number of repressed genes : ", length(downreg))

##### Graph
# df_nb_a <- data.frame(timepoint = tp, response = rep("induced", length(tp)), val = nb_a, what = "Number of genes")
# df_nb_r <- data.frame(timepoint = tp, response = rep("repressed", length(tp)), val = nb_r, what = "Number of genes")
# df_pc_a <- data.frame(timepoint = tp, response = rep("induced", length(tp)), val = pc_a, what = "Percentage of genes")
# df_pc_r <- data.frame(timepoint = tp, response = rep("repressed", length(tp)), val = pc_r, what = "Percentage of genes")
# df_nb <- rbind(df_nb_a, df_nb_r)
# df_pc <- rbind(df_pc_a, df_pc_r)
# df <- rbind(df_nb, df_pc)
# df$response <- factor(df_pc$response, level = c("repressed", "induced"))
# df$what <- factor(df$what, level = c("Percentage of genes", "Number of genes"))
# 
# secondFacet <- FALSE
# 
# ggplot(data = df, mapping = aes(fill = response, x = timepoint, y = val)) +
#   theme_minimal() +
#   xlab("Time (hour)") +
#   scale_x_continuous(breaks = tp, labels = c(0, "", "", "", "", "", 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12)) +
#   facet_grid(what ~ ., scale = "free") +
#   geom_line(data = df[df$what == "Percentage of genes",], size = 0.5, mapping = aes(color = response)) +
#   geom_point(data = df[df$what == "Percentage of genes",], size = 0.5) +
#   geom_bar(data = df[df$what == "Number of genes",], position = "stack", stat = "identity") +
#   scale_y_continuous(name = NULL, labels = function(b) {
#     if(!secondFacet) {
#       secondFacet <<- TRUE # this is a little cray (and relies on dtF seq = facet seq; works though)
#       return(paste0(round(b * 1, 0), "%"))
#     }else{
#       return(b)
#     }
#   })
# #

