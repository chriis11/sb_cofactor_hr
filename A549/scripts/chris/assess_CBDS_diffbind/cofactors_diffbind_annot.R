# setwd("/home/chris/Bureau/sb_cofactor_hr/A549")
# setwd("/Users/chris/Desktop/sb_cofactor_hr/A549")

library(ChIPseeker)
source("scripts/ckn_utils.R")

##### Load annotation database
gff_file <- "/Users/chris/Downloads/gencode.v31.basic.annotation.gff3"
gencode31_basic_chr_TxDb <- makeTxDbFromGFF(file = gff_file, format = c("gff3"),
                                            dataSource = "Gencode_v31",
                                            organism = "Homo sapiens",
                                            taxonomyId = 9606)
#####
diffbind <- load_diffbind_cofactors_peaks()

peakAnnoList <- list()
for (set in names(diffbind)) {
  region <- diffbind[[set]]
  annot <- annotatePeaks(region, tss = 3000, output = "anno", TxDb_db = gencode31_basic_chr_TxDb)
  
  peakAnnoList[[set]] <- annot
}

plotAnnoBar(peakAnnoList)
plotDistToTSS(peakAnnoList, title = "Distribution of CBDS relative to TSS")

