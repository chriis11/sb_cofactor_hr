import subprocess
import os
import time

alignment_path = "/Users/chris/Desktop/sb_cofactor_hr/MCF7/output/chip-pipeline-GRCh38/alignment"

samples = ["MCF7_CTRL_ERA_rep2", "MCF7_CTRL_MED1_rep1", "MCF7_CTRL_BRD4_rep2",
          "MCF7_E2_ERA_rep1", "MCF7_E2_MED1_rep2", "MCF7_E2_BRD4_rep1"]

i = 1
for sample in samples:
    start = time.time()

    basename = sample

    bamfile = basename + ".sorted.dup.bam"
    bedfile = basename + ".sorted.dup.bed"
    input_bam = os.path.join(alignment_path, basename, bamfile)
    output_bed = os.path.join(alignment_path, basename, bedfile)

    sub = ["bedtools", "bamtobed",
           "-i", input_bam,
           ">", output_bed]

    sub2 = " ".join(sub)
    print("##### " + str(i) + " / " + str(len(samples)))
    print(sub2)

    subprocess.call(sub2, shell=True)

    end = time.time()
    timer = end - start
    print("Conversion from BAM to BED of " + basename + " :\nDONE in " + str(timer) + " seconds")
    print("\n#################################################\n")

    i += 1
